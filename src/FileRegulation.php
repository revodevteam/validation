<?php

namespace Framework\Validation;

use Framework\Http\Notification\Notify;
use Framework\Http\Uploader\File;

class FileRegulation
{

    protected $label;
    protected $file;
    protected $optional;
    protected $passed;
    protected $notes;

    public function __construct(File $file)
    {
        $this->label = ($file->isValid()) ? $file->name : '';
        $this->file = $file;
        $this->passed = true;
        $this->notes = [];
    }

    public function label($label = false)
    {
        if ($label !== false) {
            $this->label = $this->prepareLabel($label);
            return $this;
        }

        if (!empty($this->label)) {
            return $this->prepareLabel($this->label);
        }

        return $this->prepareLabel($this->field);
    }

    protected function prepareLabel($label)
    {
        return ucwords(str_replace(['-', '_'], ' ', $label));
    }

    public function passed()
    {
        return $this->passed;
    }

    public function failureNotes()
    {
        return $this->notes;
    }

    public function additional(callable $callback)
    {
        call_user_func_array($callback, [$this]);

        return $this;
    }

    public function optional()
    {
        $this->optional = true;

        return $this;
    }

    public function required()
    {
        $this->optional = false;

        if ($this->file->isValid() === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' is required !');
        }
        return $this;
    }

    public function image()
    {
        if ($this->file->isValid() === false) {
            return $this;
        }
        if ($this->file->isImage() === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be an image!');
        }
        return $this;
    }
    
    public function extensionFilter($whitelist = [], $blacklist = []){
            
    }

    public function isArray()
    {
        if (is_null($this->data)) {
            return $this;
        }

        if (is_array($this->data) === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be multiple !');
        }
    }

    public function custom(callable $callback, ...$parameters)
    {
        if (is_callable($callback)) {
            call_user_func($callback, $this, $parameters);
        }

        return $this;
    }

    public function addNote(string $note)
    {
        $this->notes[] = $note;
    }

    public function setPassed(bool $bool = false)
    {
        $this->passed = $bool;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getField()
    {
        return $this->field;
    }

}
