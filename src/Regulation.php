<?php

namespace Framework\Validation;

use Framework\Http\Notification\Notify;

class Regulation
{

    protected $label;
    protected $field;
    protected $data;
    protected $optional;
    protected $passed;
    protected $notes;

    public function __construct($field, $data)
    {
        $this->field = $this->label = $field;
        $this->data = $data;
        $this->passed = true;
        $this->notes = [];
    }

    public function label($label = false)
    {
        if ($label !== false) {
            $this->label = $this->prepareLabel($label);
            return $this;
        }

        if (!empty($this->label)) {
            return $this->prepareLabel($this->label);
        }

        return $this->prepareLabel($this->field);
    }

    protected function prepareLabel($label)
    {
        return ucwords(str_replace(['-', '_'], ' ', $label));
    }

    public function passed()
    {
        return $this->passed;
    }

    public function failureNotes()
    {
        return $this->notes;
    }

    public function additional(callable $callback)
    {
        call_user_func_array($callback, [$this]);

        return $this;
    }

    public function optional()
    {
        $this->optional = true;

        return $this;
    }

    public function required()
    {
        $this->optional = false;

        if (is_null($this->data) || $this->data === '') {
            $this->passed = false;
            $this->addNote($this->label() . ' is required !');
        }
        return $this;
    }

    public function numeric()
    {
        if (is_null($this->data)) {
            return $this;
        }
        if (filter_var($this->data, FILTER_VALIDATE_INT) === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be a number !');
        }

        return $this;
    }

    public function email()
    {
        if (is_null($this->data)) {
            return $this;
        }
        if (filter_var($this->data, FILTER_VALIDATE_EMAIL) === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be a valid email address !');
        }

        return $this;
    }

    public function url()
    {
        if (is_null($this->data)) {
            return $this;
        }
        if (filter_var($this->data, FILTER_VALIDATE_URL) === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be a valid URL !');
        }

        return $this;
    }

    public function ip()
    {
        if (is_null($this->data)) {
            return $this;
        }
        if (filter_var($this->data, FILTER_VALIDATE_IP) === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be a valid IP !');
        }

        return $this;
    }

    public function min(int $min)
    {
        if (is_null($this->data)) {
            return $this;
        }
        if (strlen($this->data) < $min) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be atleast ' . $min . ' characters !');
        }

        return $this;
    }

    public function max(int $max)
    {
        if (is_null($this->data)) {
            return $this;
        }
        if (strlen($this->data) > $max) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be less than ' . $max . ' characters !');
        }

        return $this;
    }

    public function alphaNum()
    {
        if (is_null($this->data)) {
            return $this;
        }

        if (preg_match('/[^a-zA-Z0-9]/', $this->data)) {
            $this->passed = false;
            $this->addNote($this->label() . ' should only contain letters and numbers !');
        }

        return $this;
    }

    public function alphaNumWords()
    {
        if (is_null($this->data)) {
            return $this;
        }

        if (preg_match('/[^a-zA-Z0-9\ ]/', $this->data)) {
            $this->passed = false;
            $this->addNote($this->label() . ' should only contain letters and numbers !');
        }

        return $this;
    }

    public function matchWith(Regulation $data)
    {
        if ($this->data !== $data->getData()) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be same as ' . $data->getField() . ' !');
        }
    }

    public function unique($model, $column, $exclude = [])
    {
        if (is_object($exclude) && property_exists($exclude, $column) && $exclude->{$column} === $this->data) {
            return $this;
        } elseif (is_array($exclude) && in_array($this->data, $exclude) === true) {
            return $this;
        }

        if ($model->checkIfExists($column, $this->data) !== false) {
            $this->passed = false;
            $this->addNote($this->label() . ' already exists !');
        }

        return $this;
    }

    public function isArray()
    {
        if (is_null($this->data)) {
            return $this;
        }

        if (is_array($this->data) === false) {
            $this->passed = false;
            $this->addNote($this->label() . ' should be multiple !');
        }
    }

    public function custom(callable $callback, ...$parameters)
    {
        if (is_callable($callback)) {
            call_user_func($callback, $this, $parameters);
        }

        return $this;
    }

    public function addNote(string $note)
    {
        $this->notes[] = $note;
    }

    public function setPassed(bool $bool = false)
    {
        $this->passed = $bool;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getField()
    {
        return $this->field;
    }

}
