<?php

namespace Framework\Validation;

use Exception;
use Framework\Http\Uploader\File;
use Framework\Http\Notification\Notification;

class Validator
{

    protected $input;
    protected $notifier;
    protected $regulationSet;

    public function __construct($input, Notification $notification)
    {
        $this->input = (array) $input;
        $this->notifier = $notification;
        $this->regulationSet = [];
    }

    /**
     * 
     * @param string $name
     * @param array $arguments
     * @return \Framework\Validation\Regulation
     */
    public function __call($name, $arguments = [])
    {
        if (isset($this->input[$name])) {
            return $this->regulationSet[] = new Regulation($name, $this->input[$name]);
        } else {
            return $this->regulationSet[] = new Regulation($name, null);
        }
    }

    /**
     * 
     * @param string $name
     * @return \Framework\Validation\Regulation
     */
    public function select($name)
    {
        if (isset($this->input[$name])) {
            return $this->regulationSet[] = new Regulation($name, $this->input[$name], $this->notifier);
        } else {
            return $this->regulationSet[] = new Regulation($name, null, $this->notifier);
        }
    }

    /**
     * 
     * @param \Framework\Http\Uploader\File $file
     * @return \Framework\Validation\FileRegulation
     */
    public function file(File $file)
    {
        return $this->regulationSet[] = new FileRegulation($file, $this->notifier);
    }

    public function validate($message = true)
    {
        $isPassed = true;

        if (!empty($this->regulationSet)) {
            foreach ($this->regulationSet as $regulation) {
                if ($regulation->passed() === false) {
                    $isPassed = false;
                    if ($message) {
                        $this->warningNote($regulation->failureNotes());
                    }
                }
            }
        }

        return $isPassed;
    }

    protected function warningNote($notes)
    {
        if (empty($notes)) {
            return false;
        }
        if (is_array($notes)) {
            foreach ($notes as $note) {
                $this->notifier->warningNote($note);
            }
        } elseif (is_string($notes)) {
            $this->notifier->warningNote($notes);
        }

        return true;
    }

}
